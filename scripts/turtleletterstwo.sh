#!/usr/bin/bash

rosservice call /turtle1/set_pen 255 0 255 10 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
    -- '[0.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
    -- '[0.0, -3.0, 0.0]' '[0.0, 0.0, -3.0]'

rosservice call /spawn 8 3 0 ""
rosservice call /turtle2/set_pen 255 255 0 10 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
    -- '[2.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
    -- '[0.0, 5.0, 0.0]' '[0.0, 0.0, 5.0]'
