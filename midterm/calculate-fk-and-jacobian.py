from math import cos, pi, sin
import sympy

J_thetas = [
    -0.00020350030104410166,
    -1.554958094477147,
    2.1833591587369288,
    -1.1747402125874284,
    -0.0002580618346357255,
    2.662455766977291,
]
G_thetas = [
    -0.00010346075784539721,
    -1.3135428719917952,
    1.4271691667573938,
    -1.0670906107478118,
    -0.000356671180724355,
    3.070940044838335,
]
T_thetas = [
    -0.00015087604064056848,
    -1.2526701917446337,
    1.8867874389989723,
    -1.5424948009520056,
    -0.00026927905058027335,
    3.0237635272733723,
]

theta_dict = {"J": J_thetas, "G": G_thetas, "T": T_thetas}

d_list = [0.163, 0, 0, 0.134, 0.1, 0.1]
a_list = [0, -0.425, -0.39225, 0, 0, 0]
alpha_list = [pi / 2, 0, 0, pi / 2, -pi / 2, 0]


def calculate_transformation_matrix_from_DH_values(theta, d, a, alpha):
    A = sympy.Matrix(
        [
            [
                cos(theta),
                -cos(alpha) * sin(theta),
                sin(alpha) * sin(theta),
                a * cos(theta),
            ],
            [
                sin(theta),
                cos(theta) * cos(alpha),
                -sin(alpha) * cos(theta),
                a * sin(theta),
            ],
            [0, sin(alpha), cos(alpha), d],
            [0, 0, 0, 1],
        ]
    )
    return A


def calculate_intermediate_transformation_matrices(theta_list):
    A0 = sympy.eye(4)
    A0_matrices = [A0]
    for n, theta in enumerate(theta_list):
        A_matrix = calculate_transformation_matrix_from_DH_values(
            theta, d_list[n], a_list[n], alpha_list[n]
        )
        A0 = A0 @ A_matrix
        A0_matrices.append(A0)
    return A0_matrices


def get_overall_transformation_matrix(theta_list):
    A0_matrices = calculate_intermediate_transformation_matrices(theta_list)
    return A0_matrices[-1]


def get_z_vectors(theta_list):
    A0_matrices = calculate_intermediate_transformation_matrices(theta_list)
    z_vectors = [A0_matrix[0:3, 2] for A0_matrix in A0_matrices]
    return z_vectors


def get_o_vectors(theta_list):
    A0_matrices = calculate_intermediate_transformation_matrices(theta_list)
    o_vectors = [A0_matrix[0:3, 3] for A0_matrix in A0_matrices]
    return o_vectors


def calculate_J_v(theta_list):
    J_v = sympy.zeros(3, len(theta_list))
    z_vectors = get_z_vectors(theta_list)
    o_vectors = get_o_vectors(theta_list)
    for n, z_vector in enumerate(z_vectors[:-1]):
        J_v[:, n] = z_vector.cross(o_vectors[-1] - o_vectors[n])
    return J_v


def calculate_J_omega(theta_list):
    z_vectors = get_z_vectors(theta_list)
    J_omega = sympy.Matrix.hstack(*z_vectors[:-1])
    return J_omega


def calculate_J(theta_list):
    J_v = calculate_J_v(theta_list)
    J_omega = calculate_J_omega(theta_list)
    return sympy.Matrix.vstack(J_v, J_omega)


def pprint_matrix(matrix):
    formatted_matrix = matrix.applyfunc(lambda x: sympy.Symbol("{:.4f}".format(x)))
    sympy.pprint(formatted_matrix)


for my_initial, theta_list in theta_dict.items():
    print("My initial:", my_initial)
    print("\nForward kinematics:\n")
    pprint_matrix(get_overall_transformation_matrix(theta_list))
    print("\nJacobian:\n")
    pprint_matrix(calculate_J(theta_list))
    print("=" * 60, "\n")
